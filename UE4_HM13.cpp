﻿#include <iostream>
#include "Helpers.h"

int main()
{
    int a, b;
    std::cout << "Input a: ";
    std::cin >> a;
    std::cout << "Input b: ";
    std::cin >> b;
    std::cout << "The square of the sum of the numbers 'a' and 'b' is equal to " << square_sum(a, b) << std::endl;
}